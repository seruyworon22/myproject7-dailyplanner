﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.IO;

namespace Homework_07
{
    /// <summary>
    ////ежедневник
    /// </summary>
    class Dairy
    {
        //Лист с записями
        private ObservableCollection<DairyRecord> recordsList = new ObservableCollection<DairyRecord>();

        public ObservableCollection<DairyRecord> GetDairy
        {
            get { return recordsList; }
        }


        /// <summary>
        /// Добавить запись на лист
        /// </summary>
        /// <param name="Date"></param> Дата
        /// <param name="Name"></param> Название
        /// <param name="Description"></param>Описание
        /// <param name="Address"></param>Место
        /// <param name="Guests"></param>Гости
        public void AddRecordInDairy(DateTime Date, string Name, string Description, string Address, string Guests)
        {
            //новая запись
            DairyRecord newrecord = new DairyRecord();
            //пишем параметры
            newrecord.Record(Date, Name, Description, Address, Guests);
            //заносим на лист
            recordsList.Add(newrecord);
        }
        /// <summary>
        /// выводит запись
        /// </summary>
        /// <param name="RecIndex"></param> индекс записи
        /// <returns></returns>
        private string PrintRecord(int RecIndex)
        {
            return $"{recordsList[RecIndex].RecordDate}; {recordsList[RecIndex].RecordName}; {recordsList[RecIndex].RecordDescription}; {recordsList[RecIndex].RecordAddress}; {recordsList[RecIndex].RecordGuests} \n";
        }
        /// <summary>
        /// читает записи
        /// </summary>
        /// <param name="pathFile"></param> путь к файлу
        public void ReadRecord(string pathFile)
        {//читаем файл
            List<string> fileRecord =ReadFile(pathFile);
            for (int i=0; i< fileRecord.Count; i++)
            {//разбираем строку на слова
                string[] words = new string[5];
                words=fileRecord[i].Split(';', ',');
                //пишем на лист
                try
                {
                    AddRecordInDairy(Convert.ToDateTime(words[0]), words[1], words[2], words[3], words[4]);
                }
                catch { }
            }
        }
        /// <summary>
        /// Импорт записей
        /// </summary>
        /// <param name="pathFile"></param> файл для сохранения
        /// <param name="dstart"></param> наччало
        /// <param name="dend"></param>конец
        public void ImportRecord(string pathFile, DateTime dstart, DateTime dend)
        {   // запись в файл
            using (FileStream fstream = new FileStream(pathFile, FileMode.Create))
            {
                for (int i = 0; i < recordsList.Count; i++)
                {
                    if (recordsList[i].RecordDate <= dend && recordsList[i].RecordDate >= dstart)
                    // преобразуем строку в байты
                    {
                        byte[] array = System.Text.Encoding.Default.GetBytes(PrintRecord(i));
                        // запись массива байтов в файл
                        fstream.Write(array, 0, array.Length);
                    }
                }

            }

        }

        /// <summary>
        /// сортирует запись
        /// </summary>
        /// <param name="field"></param> поля для сортировки
        public void sortRecord(int field)
        {

            List<DairyRecord> sortedList = new List<DairyRecord>();

            switch (field)
            {
                case 0:
                    sortedList=recordsList.OrderBy(u => u.RecordDate).ToList();
                    break;
                case 1:
                    sortedList = recordsList.OrderBy(u => u.RecordName).ToList();
                    break;
                case 2:
                    sortedList = recordsList.OrderBy(u => u.RecordDescription).ToList();
                    break;
                case 3:
                    sortedList = recordsList.OrderBy(u => u.RecordAddress).ToList();
                    break;
                case 4:
                    sortedList = recordsList.OrderBy(u => u.RecordGuests).ToList();
                    break;
            }

            for(int q=0; q< sortedList.Count; q++)
            {
                recordsList[q] = sortedList[q];

            }
                

        }


        /// <summary>
        /// читает файл
        /// </summary>
        /// <param name="pathfile"></param>путь
        /// <returns>содержимое файла</returns>
        private List<string> ReadFile(string pathfile)
        {
            List<string> lines = new List<string>();
            FileStream file = new FileStream(pathfile, FileMode.Open);
            StreamReader readFile = new StreamReader(file, Encoding.GetEncoding(1251));
            while (!readFile.EndOfStream)
            {
                lines.Add(readFile.ReadLine());
            }
            readFile.Close();
            return lines;
        }


    }
}
