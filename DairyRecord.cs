﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_07
{
    /// <summary>
    /// Запись ежедневника
    /// </summary>
    class DairyRecord
    {
        /// <summary>
        /// дата события
        /// </summary>
        private DateTime Date ;
        /// <summary>
        /// наименование события
        /// </summary>
        private string Name;
        /// <summary>
        /// описание
        /// </summary>
        private string Description;
        /// <summary>
        /// место проведения
        /// </summary>
        private string Address;
        /// <summary>
        /// кто еще должет быть на событии
        /// </summary>
        private string Guests;
        /// <summary>
        /// дата события
        /// </summary>
        public DateTime RecordDate
        {
            set { Date = value; }
            get { return Date; }
        }
        /// <summary>
        /// наименование события
        /// </summary>
        public string RecordName
        {
            set { Name = value; }
            get { return Name; }
        }
        /// <summary>
        /// описание события
        /// </summary>
        public string RecordDescription
        {
            set { Description = value; }
            get { return Description; }
        }
        /// <summary>
        /// место проведения
        /// </summary>
        public string RecordAddress
        {
            set { Address = value; }
            get { return Address; }
        }
        /// <summary>
        /// кто еще должет быть на событии
        /// </summary>
        public string RecordGuests
        {
            set { Guests = value; }
            get { return Guests; }
        }
        /// <summary>
        /// Создание записи
        /// </summary>
        /// <param name="Date"></param> Дата
        /// <param name="Name"></param> Название
        /// <param name="Description"></param>Описание
        /// <param name="Address"></param>Место
        /// <param name="Guests"></param>Гости
        public void Record(DateTime Date,string  Name, string Description, string Address, string Guests)
        {
            this.Date = Date;
            this.Name = Name;
            this.Description = Description;
            this.Address = Address;
            this.Guests = Guests;

        }


    }
}
