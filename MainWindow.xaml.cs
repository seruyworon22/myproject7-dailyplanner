﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Microsoft.Win32;

namespace Homework_07
{
    /// Разработать ежедневник.
    /// В ежедневнике реализовать возможность 
    /// - создания
    /// - удаления
    /// - реактирования 
    /// записей
    /// 
    /// В отдельной записи должно быть не менее пяти полей
    /// 
    /// Реализовать возможность 
    /// - Загрузки даннах из файла
    /// - Выгрузки даннах в файл
    /// - Добавления данных в текущий ежедневник из выбранного файла
    /// - Импорт записей по выбранному диапазону дат
    /// - Упорядочивания записей ежедневника по выбранному полю
    /// 
    public partial class MainWindow : Window
    {
        //мой ежедневник
        Dairy myDairy = new Dairy();
        /// <summary>
        /// инициализация
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            GridRecord.ItemsSource = myDairy.GetDairy;
            DatePicker1.SelectedDate = DateTime.Now.AddDays(-7);
            DatePicker2.SelectedDate = DateTime.Now;
        }
        /// <summary>
        /// загрузка данных из файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddRecord_Click(object sender, RoutedEventArgs e)
        {//диалог по выбору файла
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {//пишем на лист
                myDairy.ReadRecord(dialog.FileName);
            }
           
        }
        /// <summary>
        /// импорт записей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_ImportDairy_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog();
            if (dialog.ShowDialog() == true)
            {//пишем в файл
                myDairy.ImportRecord(dialog.FileName, Convert.ToDateTime( DatePicker1.Text), Convert.ToDateTime(DatePicker2.Text));
            }
        }
        /// <summary>
        /// сортировка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonSort_Click(object sender, RoutedEventArgs e)
        {
            myDairy.sortRecord(CBSort.SelectedIndex);
        }
    }


}
